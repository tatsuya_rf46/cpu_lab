let rec foo x =
    let y = read_int () in
    print_int y
in
let _ = foo 3 in
0
