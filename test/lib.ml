let rec divByTen_sub x =
    let (i, n) = x in
    if n < 10 then
        (i, n)
    else
        divByTen_sub (i+1, n-10) in
let rec divByTen n = divByTen_sub (0, n) in
let rec print_int n =
    if n < 10 then
        print_char (n+48)
    else
        (let (i, m) = divByTen n in
         print_int i;
         print_char (m+48)) in
