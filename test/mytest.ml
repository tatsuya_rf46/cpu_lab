let rec iter index =
    if index >= 0 then
        sum.(0) <- sum.(0) +. 1.0;
        iter (index - 1)
    else
        ()
in
let _ = iter 99 in
0
