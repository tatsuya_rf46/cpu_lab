let rec iter index =
    if index >= 0 then
        let x = ref0.(0) in
        ref1.(0) <- ref1.(0) + index;
        sum.(0) <- sum.(0) +. 1.0;
        iter (index - 1)
    else
        ()
in
let _ = iter 15 in
0
