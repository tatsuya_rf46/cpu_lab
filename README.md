Install
-------
`git clone https://gitlab.com/tatsuya_rf46/cpu_lab.git`  
`git submodule update --init --recursive`  

Update
------
`git pull`  
`git submodule sync --recursive`  
`git submodule update --recursive`

Make
-------
`make`(default)  
Compile, assemble, and link all files(testcode/, shootout/mandelbrot.ml, raytracer/minrt.ml).  
`make $(file).s`  
Compile the file.  
`make $(file)_instr.mem`  
Compile, assemble, and link the file.  
ex)  `make raytracer/minrt_instr.mem` generates 'raytracer/minrt_instr.mem', 'raytracer/minrt_data.mem', and 'raytracer/minrt_debug.txt'.  
`make clean`  
Clean all built files.

Parallel function
-----------------
"ter_trace_diffuse_rays"
