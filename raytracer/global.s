min_caml_n_objects:
    1, 0x0
min_caml_dummy:
min_caml_objects_t:
    4, 0x0
    2, min_caml_dummy
    1, 0x0
    4, min_caml_dummy
min_caml_objects:
    60, min_caml_objects_t
min_caml_screen:
    3, 0x0
min_caml_viewpoint:
    3, 0x0
min_caml_light:
    3, 0x0
min_caml_beam:
    3, 0x437f0000
min_caml_and_net_t:
    1, 0xffffffff
min_caml_and_net:
    50, min_caml_and_net_t
min_caml_or_net_t:
    1, min_caml_and_net_t
min_caml_or_net:
    1, min_caml_or_net_t
min_caml_solver_dist:
    1, 0x0
min_caml_intsec_rectside:
    1, 0x0
min_caml_tmin:
    1, 0x4e6e6b28
min_caml_intersection_point:
    3, 0x0
min_caml_intersected_object_id:
    1, 0x0
min_caml_nvector:
    3, 0x0
min_caml_texture_color:
    3, 0x0
min_caml_diffuse_ray:
    3, 0x0
min_caml_rgb:
    3, 0x0
min_caml_image_size:
    2, 0x0
min_caml_image_center:
    2, 0x0
min_caml_scan_pitch:
    1, 0x0
min_caml_startp:
    3, 0x0
min_caml_startp_fast:
    3, 0x0
min_caml_screenx_dir:
    3, 0x0
min_caml_screeny_dir:
    3, 0x0
min_caml_screenz_dir:
    3, 0x0
min_caml_ptrace_dirvec:
    3, 0x0
min_caml_dirvecs_dummy_vs:
min_caml_dirvecs:
    5, min_caml_dirvecs_dummy_vs
min_caml_light_dirvec_dummyf2:
min_caml_light_dirvec_v3:
    3, 0x0
min_caml_light_dirvec_contsts:
    60, min_caml_light_dirvec_dummyf2
min_caml_light_dirvec:
    1, min_caml_light_dirvec_v3
    1, min_caml_light_dirvec_contsts
min_caml_reflections_dummyf3:
min_caml_reflections_dummyff3:
min_caml_reflections_dummydv:
    1, min_caml_reflections_dummyf3
    1, min_caml_reflections_dummyff3
min_caml_reflections_t:
    1, 0x0
    1, min_caml_reflections_dummydv
    1, 0x0
min_caml_reflections:
    180, min_caml_reflections_t
min_caml_n_reflections:
    1, 0x0
