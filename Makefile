COMPILER = ./my-min-caml/my-min-caml
ASSEMBLER = ./assembler/assembler
LIB = ./my-min-caml/lib.s

TEST = mytest mytest2 mytest3 ack adder cls-bug2 cls-bug cls-rec cls-reg-bug even-odd fib float funcomp \
	  	   gcd inprod-loop inprod inprod-rec join-reg2 join-reg join-stack2 join-stack3 join-stack \
	  	   manyargs matmul-flat matmul non-tail-if2 non-tail-if print shuffle spill2 spill3 spill \
	  	   sum sum-tail toomanyargs
MANDELBROT = shootout/mandelbrot
MINRT = raytracer/minrt

.PHONY: all
all: assemble

.PHONY: assemble
assemble: $(TEST:%=test/%_instr.mem)

test/%_instr.mem: $(ASSEMBLER) test/%.s
	$(ASSEMBLER) test/$*

$(MANDELBROT)_instr.mem: $(ASSEMBLER) $(MANDELBROT).s
	$(ASSEMBLER) $(MANDELBROT)

$(MINRT)_instr.mem: $(ASSEMBLER) $(MINRT).s
	$(ASSEMBLER) $(MINRT)

$(ASSEMBLER):
	make -C ./assembler

.PHONY: compile
compile: $(TEST:%=test/%.s)

test/%.s: $(COMPILER) test/%.ml
	$(COMPILER) -l ./test/lib.ml -g ./test/global.s test/$*
	cat $(LIB) >> test/$*.s

$(MANDELBROT).s: $(COMPILER) $(MANDELBROT).ml
	$(COMPILER) $(MANDELBROT)
	cat $(LIB) >> $(MANDELBROT).s

$(MINRT).s: $(COMPILER) $(MINRT).ml
	$(COMPILER) -l ./my-min-caml/lib.ml -g ./raytracer/global.s $(MINRT)
	cat $(LIB) >> $(MINRT).s

$(COMPILER):
	make my-min-caml -C ./my-min-caml

.PHONY: clean
clean:
	make clean -C ./my-min-caml
	make clean -C ./assembler
	rm -f \
		$(TEST:%=test/%.s) $(TEST:%=test/%.mlo) \
		$(TEST:%=test/%_dbg.txt) \
		$(TEST:%=test/%_syntax.txt) $(TEST:%=test/%_kNormal.txt) \
		$(TEST:%=test/%_alpha.txt) $(TEST:%=test/%_vir.txt) $(TEST:%=test/%_loop.txt) \
		$(TEST:%=test/%_blk.txt) $(TEST:%=test/%_opt.txt) $(TEST:%=test/%_reg.txt) \
		$(TEST:%=test/%_asm.txt) $(TEST:%=test/%_sch.txt) \
		$(TEST:%=test/%_instr.mem) $(TEST:%=test/%_instr0.mem) \
		$(TEST:%=test/%_instr1.mem) $(TEST:%=test/%_instr2.mem) \
		$(TEST:%=test/%_instr3.mem) \
		$(TEST:%=test/%_data.mem) $(TEST:%=test/%_debug.txt) \
		$(MANDELBROT).s $(MANDELBROT).mlo \
		$(MANDELBROT)_syntax.txt $(MANDELBROT)_kNormal.txt \
		$(MANDELBROT)_alpha.txt $(MANDELBROT)_vir.txt $(MANDELBROT)_loop.txt\
		$(MANDELBROT)_blk.txt $(MANDELBROT)_reg.txt \
		$(MANDELBROT)_instr.mem $(MANDELBROT)_data.mem $(MANDELBROT)_debug.txt \
		$(MINRT).s $(MINRT).mlo \
		$(MINRT)_dbg.txt \
		$(MINRT)_syntax.txt $(MINRT)_kNormal.txt \
		$(MINRT)_alpha.txt $(MINRT)_vir.txt $(MINRT)_loop.txt \
		$(MINRT)_blk.txt $(MINRT)_opt.txt $(MINRT)_reg.txt \
		$(MINRT)_asm.txt $(MINRT)_sch.txt \
		$(MINRT)_instr.mem \
		$(MINRT)_instr0.mem \
		$(MINRT)_instr1.mem \
		$(MINRT)_instr2.mem \
		$(MINRT)_instr3.mem \
		$(MINRT)_data.mem $(MINRT)_debug.txt
